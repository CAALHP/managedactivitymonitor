﻿using System.Diagnostics;
using nfclib;

namespace nfc_lib_test
{
    class Program
    {
        static void Main(string[] args)
        {
            //Debugger.Launch();
            var activityMonitor = new ManagedUW101NFC();
            activityMonitor.Run();
        }
    }
}
