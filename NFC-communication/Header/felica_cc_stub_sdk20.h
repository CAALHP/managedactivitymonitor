/**
 * \brief    The stub functions binding felica_cc to sdk20.
 * \date     2007/03/20
 * \author   Copyright 2007 Sony Corporation
 */

#include "ics_types.h"
#include "ics_hwdev.h"
#include "felica_cc_stub.h"

#ifndef FELICA_CC_STUB_SDK20_H_
#define FELICA_CC_STUB_SDK20_H_

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Constant
 */

#define FELICA_CC_STUB_SDK20_MAX_COMMAND_LEN            240
#define FELICA_CC_STUB_SDK20_MAX_RESPONSE_LEN           240

/*
 * Prototype declaration
 */

UINT32 felica_cc_stub_sdk20_initialize(
    felica_cc_devf_t* devf,
    ICS_HW_DEVICE* dev);

#ifdef __cplusplus
}
#endif

#endif /* !FELICA_CC_STUB_SDK20_H_ */
