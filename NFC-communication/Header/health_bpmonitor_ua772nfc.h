/**
 * \brief    The header file for NFC Healthcare Library. (UA-772NFC)
 * \author   Copyright 2012,2013 Sony Corporation
 */

#include "ics_types.h"
#include "health.h"
#include "health_bpmonitor.h"

#ifndef HEALTH_BPMONITOR_UA772NFC_H_
#define HEALTH_BPMONITOR_UA772NFC_H_

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Constant
 */

#define HEALTH_NAME_BPMONITOR_UA772NFC                  "UA-772NFC"

#define HEALTH_BPMONITOR_UA772NFC_MAX_LOG_DATA_LEN      100
#define HEALTH_BPMONITOR_UA772NFC_LOG_DATA_TYPE \
    health_bpmonitor_ua772nfc_data_t

#define HEALTH_BPMONITOR_UA772NFC_SERIAL_NUM_MAX_LEN    16
#define HEALTH_BPMONITOR_UA772NFC_PRODUCT_CODE_MAX_LEN  16

#define HEALTH_BPMONITOR_UA772NFC_FLAG_IRREGULAR_HEARTBEAT \
    (HEALTH_BPMONITOR_FLAG_PRODUCT << 0)
#define HEALTH_BPMONITOR_UA772NFC_FLAG_SLOW_HEARTBEAT \
    (HEALTH_BPMONITOR_FLAG_PRODUCT << 1)
#define HEALTH_BPMONITOR_UA772NFC_FLAG_FAST_HEARTBEAT \
    (HEALTH_BPMONITOR_FLAG_PRODUCT << 2)

#define HEALTH_IOCTL_UA772NFC_SET_CLOCK                 "set_clock"
#define HEALTH_IOCTL_UA772NFC_CLEAR_MEMORY              "clear_memory"

/*
 * Type and structure
 */

typedef struct health_bpmonitor_ua772nfc_aux_data_t {
    char serial_num[HEALTH_BPMONITOR_UA772NFC_SERIAL_NUM_MAX_LEN + 1];
    char product_code[HEALTH_BPMONITOR_UA772NFC_PRODUCT_CODE_MAX_LEN + 1];
} health_bpmonitor_ua772nfc_aux_data_t;

typedef struct health_ioctl_ua772nfc_set_clock_t {
    const health_devf_t* devf;
    const felica_card_t* card;
    ics_calendar_t clock;
} health_ioctl_ua772nfc_set_clock_t;

typedef struct health_ioctl_ua772nfc_clear_memory_t {
    const health_devf_t* devf;
    const felica_card_t* card;
} health_ioctl_ua772nfc_clear_memory_t;

/*
 * Prototype declaration
 */

const struct health_tag_t* health_bpmonitor_ua772nfc_get_tag(void);

#ifdef __cplusplus
}
#endif

#endif /* !HEALTH_BPMONITOR_UA772NFC_H_ */
