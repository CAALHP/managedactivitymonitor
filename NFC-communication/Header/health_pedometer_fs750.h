/**
 * \brief    The header file for NFC Healthcare Library. (FS-750)
 * \author   Copyright 2013 Sony Corporation
 */

#include "ics_types.h"
#include "health.h"
#include "health_pedometer.h"

#ifndef HEALTH_PEDOMETER_FS750_H_
#define HEALTH_PEDOMETER_FS750_H_

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Constant
 */

#define HEALTH_NAME_PEDOMETER_FS750                        "FS-750"

#define HEALTH_PEDOMETER_FS750_NORMAL_LOG_DATA_LEN         15
#define HEALTH_PEDOMETER_FS750_LONG_DAILY_LOG_DATA_LEN     91
#define HEALTH_PEDOMETER_FS750_TWO_MINUTES_LOG_DATA_LEN    720
#define HEALTH_PEDOMETER_FS750_LOG_DATA_TYPE \
    health_pedometer_fs750_data_t

#define HEALTH_PEDOMETER_FS750_PEDOMETER_ID_MAX_LEN        10

#define HEALTH_READ_TYPE_FS750_LONG_DAILY                  2
#define HEALTH_READ_TYPE_FS750_TWO_MINUTES                 3

#define HEALTH_IOCTL_FS750_SET_MODE           "set_mode"
#define HEALTH_IOCTL_FS750_CLEAR_MEMORY       "clear_memory"  /* obsolete */

/*
 * Type and structure
 */

typedef struct health_pedometer_fs750_aux_data_t {
    char pedometer_id[HEALTH_PEDOMETER_FS750_PEDOMETER_ID_MAX_LEN + 1];
    INT16 sex;               /* Registered sex of the user. (0:male, 1:female) */
    INT16 age;               /* Registered age of the user. */
    UINT16 weight;           /* Registered weight of the user. (g) */
    UINT16 height;           /* Registered height of the user. (mm) */
    ics_calendar_t clock;    /* Registered clock of pedometer */
} health_pedometer_fs750_aux_data_t;

typedef struct health_pedometer_fs750_data_t {
    health_pedometer_data_t common;
    INT32 total_calorie;        /* Total calorie. (in unit of a calorie) */
    INT8 attitude;               /* Attitude. */
    INT8 momentum;              /* Momentum. */
} health_pedometer_fs750_data_t;

typedef struct health_pedometer_fs750_read_param_t {
    UINT8 ndays;                /* Number of day to read */
    ics_calendar_t tm;
} health_pedometer_fs750_read_param_t;

typedef struct health_ioctl_fs750_set_mode_t {
    const health_devf_t* devf;
    const felica_card_t* card;
    UINT8 sleep_mode;  /* Sleep mode Flag (0:valid, 1:invalid) */
    UINT8 modint_mets; /* Threshold of METs for moderate-intensity activity
                          (in unit of 0.1) */
} health_ioctl_fs750_set_mode_t;

/* obsolete */
typedef struct health_ioctl_fs750_clear_memory_t {
    const health_devf_t* devf;
    const felica_card_t* card;
} health_ioctl_fs750_clear_memory_t;

/*
 * Prototype declaration
 */

const struct health_tag_t* health_pedometer_fs750_get_tag(void);

#ifdef __cplusplus
}
#endif

#endif /* !HEALTH_PEDOMETER_FS750_H_ */
