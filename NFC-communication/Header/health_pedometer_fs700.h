/**
 * \brief    The header file for NFC Healthcare Library. (FS-700)
 * \date     2012/03/30
 * \author   Copyright 2011,2012 Sony Corporation
 */

#include "ics_types.h"
#include "health.h"
#include "health_pedometer.h"

#ifndef HEALTH_PEDOMETER_FS700_H_
#define HEALTH_PEDOMETER_FS700_H_

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Constant
 */

#define HEALTH_NAME_PEDOMETER_FS700                     "FS-700"

#define HEALTH_PEDOMETER_FS700_NORMAL_LOG_DATA_LEN      15
#define HEALTH_PEDOMETER_FS700_DAILY_LOG_DATA_LEN       15
#define HEALTH_PEDOMETER_FS700_HOURLY_LOG_DATA_LEN      24
#define HEALTH_PEDOMETER_FS700_LOG_DATA_TYPE \
    health_pedometer_fs700_data_t

#define HEALTH_PEDOMETER_FS700_PEDOMETER_ID_MAX_LEN     10
#define HEALTH_PEDOMETER_FS700_PRODUCT_CODE_MAX_LEN     16

#define HEALTH_READ_TYPE_FS700_DAILY                    2
#define HEALTH_READ_TYPE_FS700_HOURLY                   3

/*
 * Type and structure
 */

typedef struct health_pedometer_fs700_aux_data_t {
    char pedometer_id[HEALTH_PEDOMETER_FS700_PEDOMETER_ID_MAX_LEN + 1];
    char product_code[HEALTH_PEDOMETER_FS700_PRODUCT_CODE_MAX_LEN + 1];
    INT32 weight;       /* Registered weight of the user. */
    INT32 height;       /* Registered height of the user. */
} health_pedometer_fs700_aux_data_t;

typedef struct health_pedometer_fs700_data_t {
    health_pedometer_data_t common;
    INT32 jogging_step;         /* The number of jogging steps. */
    INT32 total_calorie;        /* Total calorie. (in unit of a calorie) */
    INT32 burning;              /* The amount of body fat burning.
                                 * (in unit of a gram) */
} health_pedometer_fs700_data_t;

/*
 * Prototype declaration
 */

const struct health_tag_t* health_pedometer_fs700_get_tag(void);

#ifdef __cplusplus
}
#endif

#endif /* !HEALTH_PEDOMETER_FS700_H_ */
