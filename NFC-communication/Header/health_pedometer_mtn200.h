/**
 * \brief    The header file for NFC Healthcare Library. (MTN-200)
 * \author   Copyright 2011,2012,2013 Sony Corporation
 */

#include "ics_types.h"
#include "health.h"
#include "health_pedometer.h"

#ifndef HEALTH_PEDOMETER_MTN200_H_
#define HEALTH_PEDOMETER_MTN200_H_

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Constant
 */

#define HEALTH_NAME_PEDOMETER_MTN200                    "MTN-200"

#define HEALTH_PEDOMETER_MTN200_NORMAL_LOG_DATA_LEN     15
#define HEALTH_PEDOMETER_MTN200_DAILY_LOG_DATA_LEN      15
#define HEALTH_PEDOMETER_MTN200_HOURLY_LOG_DATA_LEN     24
#define HEALTH_PEDOMETER_MTN200_LONG_DAILY_LOG_DATA_LEN 91
#define HEALTH_PEDOMETER_MTN200_LOG_DATA_TYPE \
    health_pedometer_mtn200_data_t

#define HEALTH_PEDOMETER_MTN200_PEDOMETER_ID_MAX_LEN    16
#define HEALTH_PEDOMETER_MTN200_PRODUCT_CODE_MAX_LEN    16

#define HEALTH_READ_TYPE_MTN200                         2

#define HEALTH_PEDOMETER_MTN200_F_READ_NORMAL           (1 << 0)
#define HEALTH_PEDOMETER_MTN200_F_READ_DAILY            (1 << 1)
#define HEALTH_PEDOMETER_MTN200_F_READ_HOURLY           (1 << 2)
#define HEALTH_PEDOMETER_MTN200_F_READ_LONG_DAILY       (1 << 3)
#define HEALTH_PEDOMETER_MTN200_F_SET_PERSONAL_DATA     (1 << 8)
#define HEALTH_PEDOMETER_MTN200_F_SET_CLOCK             (1 << 9)

/*
 * Type and structure
 */

typedef struct health_pedometer_mtn200_aux_data_t {
    char pedometer_id[HEALTH_PEDOMETER_MTN200_PEDOMETER_ID_MAX_LEN + 1];
    char product_code[HEALTH_PEDOMETER_MTN200_PRODUCT_CODE_MAX_LEN + 1];
    ics_calendar_t clock;       /* Clock of the pedometer. */
    INT32 weight;       /* Registered weight of the user. */
    INT32 height;       /* Registered height of the user. */
    INT32 age;          /* Registered age of the user. */
    UINT8 sex;          /* Registered sex of the user. (0:female, 1:male) */
    UINT8 low_battery_flag;     /* Battery status. (0:fine, 1:low) */
    UINT16 software_version;    /* Software version of the device. */
} health_pedometer_mtn200_aux_data_t;

typedef struct health_pedometer_mtn200_data_t {
    health_pedometer_data_t common;
    INT32 total_calorie;        /* Total calorie. (in unit of a calorie) */
    INT32 burning;              /* The amount of body fat burning.
                                 * (in unit of a gram) */
} health_pedometer_mtn200_data_t;

typedef struct health_pedometer_mtn200_read_param_t {
    UINT32 flags;
    UINT32 ndays;
    ics_calendar_t clock;
    INT32 weight;
    INT32 height;
    INT32 age;
    UINT8 sex;
} health_pedometer_mtn200_read_param_t;

/*
 * Prototype declaration
 */

const struct health_tag_t* health_pedometer_mtn200_get_tag(void);

#ifdef __cplusplus
}
#endif

#endif /* !HEALTH_PEDOMETER_MTN200_H_ */
