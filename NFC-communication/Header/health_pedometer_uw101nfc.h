/**
 * \brief    The header file for NFC Healthcare Library. (UW-101NFC)
 * \date     2011/12/25
 * \author   Copyright 2011 Sony Corporation
 */

#include "ics_types.h"
#include "health.h"
#include "health_pedometer.h"

#ifndef HEALTH_PEDOMETER_UW101NFC_H_
#define HEALTH_PEDOMETER_UW101NFC_H_

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Constant
 */

#define HEALTH_NAME_PEDOMETER_UW101NFC                  "UW-101NFC"

#define HEALTH_PEDOMETER_UW101NFC_LOG_DATA_LEN          15
#define HEALTH_PEDOMETER_UW101NFC_LOG_DATA_TYPE \
    health_pedometer_uw101nfc_data_t

#define HEALTH_PEDOMETER_UW101NFC_PEDOMETER_ID_MAX_LEN  16
#define HEALTH_PEDOMETER_UW101NFC_PRODUCT_CODE_MAX_LEN  16

#define HEALTH_PEDOMETER_UW101NFC_UNITS_SI              0x00
#define HEALTH_PEDOMETER_UW101NFC_UNITS_FPS             0x01

/*
 * Type and structure
 */

typedef struct health_pedometer_uw101nfc_aux_data_t {
    char pedometer_id[HEALTH_PEDOMETER_UW101NFC_PEDOMETER_ID_MAX_LEN + 1];
    char product_code[HEALTH_PEDOMETER_UW101NFC_PRODUCT_CODE_MAX_LEN + 1];
    INT32 units;        /* Setting of device whether SI or foot-pound-second.
                         * Even if the device is set to foot-pound-second
                         * unit system, distances are in unit of a meter.
                         */
} health_pedometer_uw101nfc_aux_data_t;

typedef struct health_pedometer_uw101nfc_data_t {
    health_pedometer_data_t common;
    INT32 distance;     /* Walked distance. (in unit of a meter) */
} health_pedometer_uw101nfc_data_t;

/*
 * Prototype declaration
 */

const struct health_tag_t* health_pedometer_uw101nfc_get_tag(void);

#ifdef __cplusplus
}
#endif

#endif /* !HEALTH_PEDOMETER_UW101NFC_H_ */
