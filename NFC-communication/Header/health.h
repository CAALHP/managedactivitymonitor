/**
 * \brief    The header file for NFC Healthcare Library.
 * \date     2011/12/25
 * \author   Copyright 2011 Sony Corporation
 */

#include "ics_types.h"
#include "felica_card.h"
#include "felica_cc_stub.h"

#ifndef HEALTH_H_
#define HEALTH_H_

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Constant
 */

#define HEALTH_NFC_DYNAMIC_TAG_SYSTEM_CODE      0xfee1
#define HEALTH_NDEF_TYPE3_TAG_SYSTEM_CODE       0x12fc

#define HEALTH_READ_TYPE_NORMAL                 1

#define HEALTH_IOCTL_HAS_CAPABILITY             "has_capability"
#define  HEALTH_CAPABILITY_GET_LOG_DATA_AT      "get_log_data_at"
#define  HEALTH_CAPABILITY_GET_ID               "get_id"
#define  HEALTH_CAPABILITY_GET_MODEL_NAME       "get_model_name"
#define HEALTH_IOCTL_GET_LOG_DATA_AT            "get_log_data_at"
#define HEALTH_IOCTL_GET_ID                     "get_id"
#define HEALTH_IOCTL_GET_MODEL_NAME             "get_model_name"

/*
 * Type and structure
 */

typedef struct health_devf_t {
    felica_cc_devf_t* felica_devf;
} health_devf_t;

struct health_tag_t;

typedef struct health_tag_data_t {
    const struct health_tag_t* tag;
    UINT32 read_type;
    void* read_param;
    void* aux_data;
    void* log_data;
    UINT32 log_len;
} health_tag_data_t;

typedef struct health_ioctl_get_log_data_at_t {
    UINT32 index;
    void* log_data;
} health_ioctl_get_log_data_at_t;

/*
 * Prototype declaration
 */

const char* health_get_version(void);

UINT32 health_tag_read(
    const health_devf_t* devf,
    UINT16 system_code,
    const felica_card_t* card,
    UINT32 tag_data_len,
    health_tag_data_t* tag_data,
    health_tag_data_t** read_data);
UINT32 health_tag_ioctl(
    health_tag_data_t* read_data,
    const char* request,
    void* arg);

const char* health_tag_get_category(
    const health_tag_data_t* read_data);
const char* health_tag_get_name(
    const health_tag_data_t* read_data);

UINT32 health_utl_get_tag_data_size(
    UINT32* buf_size);
UINT32 health_utl_set_tag_data(
    void* buf,
    UINT32* tag_data_len);

#ifdef __cplusplus
}
#endif

#endif /* !HEALTH_H_ */
