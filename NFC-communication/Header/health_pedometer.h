/**
 * \brief    The header file for NFC Healthcare Library. (Pedometer)
 * \date     2011/12/25
 * \author   Copyright 2011 Sony Corporation
 */

#include "ics_types.h"
#include "utl_calendar.h"
#include "health.h"

#ifndef HEALTH_PEDOMETER_H_
#define HEALTH_PEDOMETER_H_

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Constant
 */

#define HEALTH_CATEGORY_PEDOMETER                       "Pedometer"

#define HEALTH_PEDOMETER_LOG_DATA_LEN                   15

/*
 * Type and structure
 */

typedef struct health_pedometer_data_t {
    ics_calendar_t tm;  /* The measured time. */
    INT32 step;         /* The number of steps. */
    INT32 active_step;  /* The number of active steps. */
    INT32 calorie;      /* Consumed calorie. (in unit of a calorie) */
    INT32 ex;           /* EX (exercise) value introduced by Japanese
                         * Ministry of Health, Labour and Welfare.
                         * (in unit of 0.001EX) */
} health_pedometer_data_t;

#ifdef __cplusplus
}
#endif

#endif /* !HEALTH_PEDOMETER_H_ */
