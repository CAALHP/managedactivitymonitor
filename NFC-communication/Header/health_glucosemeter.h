/**
 * \brief    The header file for NFC Healthcare Library. (Glucose Meter)
 * \date     2012/5/11
 * \author   Copyright 2011,2012 Sony Corporation
 */

#include "ics_types.h"
#include "utl_calendar.h"
#include "health.h"

#ifndef HEALTH_GLUCOSEMETER_H_
#define HEALTH_GLUCOSEMETER_H_

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Constant
 */

#define HEALTH_CATEGORY_GLUCOSEMETER                    "GlucoseMeter"

#define HEALTH_GLUCOSEMETER_UNIT_MG_PER_DL              0
#define HEALTH_GLUCOSEMETER_UNIT_MMOL_PER_L             1

#define HEALTH_GLUCOSEMETER_MEAL_PREPRANDIAL            (1UL << 1)
#define HEALTH_GLUCOSEMETER_MEAL_POSTPRANDIAL           (1UL << 2)

#define HEALTH_GLUCOSEMETER_FLAG_PRODUCT                (1UL << 24)

/*
 * Type and structure
 */

typedef struct health_glucosemeter_data_t {
    ics_calendar_t tm;  /* The measured time. */
    INT32 blood_glucose;/* Blood glucose level. */
    INT32 unit;         /* Unit of blood_glucose. */
    UINT32 flags;       /* Information when the data was measured. */
} health_glucosemeter_data_t;

#ifdef __cplusplus
}
#endif

#endif /* !HEALTH_GLUCOSEMETER_H_ */
