/**
 * \brief    The header file for NFC Healthcare Library. (FS-500A)
 * \date     2012/03/29
 * \author   Copyright 2011,2012 Sony Corporation
 */

#include "ics_types.h"
#include "health.h"
#include "health_pedometer.h"

#ifndef HEALTH_PEDOMETER_FS500A_H_
#define HEALTH_PEDOMETER_FS500A_H_

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Constant
 */

#define HEALTH_NAME_PEDOMETER_FS500A                    "FS-500A"

#define HEALTH_PEDOMETER_FS500A_LOG_DATA_LEN            15
#define HEALTH_PEDOMETER_FS500A_LOG_DATA_TYPE \
    health_pedometer_data_t

#define HEALTH_PEDOMETER_FS500A_PEDOMETER_ID_MAX_LEN    10

/*
 * Type and structure
 */

typedef struct health_pedometer_fs500a_aux_data_t {
    char pedometer_id[HEALTH_PEDOMETER_FS500A_PEDOMETER_ID_MAX_LEN + 1];
    INT32 weight;       /* Registered weight of the user. */
    INT32 stride;       /* Registered stride of the user. */
} health_pedometer_fs500a_aux_data_t;

/*
 * Prototype declaration
 */

const struct health_tag_t* health_pedometer_fs500a_get_tag(void);

#ifdef __cplusplus
}
#endif

#endif /* !HEALTH_PEDOMETER_FS500A_H_ */
