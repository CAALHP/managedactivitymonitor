/**
 * \brief    The header file for NFC Healthcare Library. (EX-950)
 * \date     2012/03/29
 * \author   Copyright 2011,2012 Sony Corporation
 */

#include "ics_types.h"
#include "health.h"
#include "health_pedometer.h"

#ifndef HEALTH_PEDOMETER_EX950_H_
#define HEALTH_PEDOMETER_EX950_H_

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Constant
 */

#define HEALTH_NAME_PEDOMETER_EX950                     "EX-950"

#define HEALTH_PEDOMETER_EX950_LOG_DATA_LEN             15
#define HEALTH_PEDOMETER_EX950_LOG_DATA_TYPE \
    health_pedometer_data_t

#define HEALTH_PEDOMETER_EX950_PEDOMETER_ID_MAX_LEN     10

/*
 * Type and structure
 */

typedef struct health_pedometer_ex950_aux_data_t {
    char pedometer_id[HEALTH_PEDOMETER_EX950_PEDOMETER_ID_MAX_LEN + 1];
    INT32 weight;       /* Registered weight of the user. */
    INT32 stride;       /* Registered stride of the user. */
} health_pedometer_ex950_aux_data_t;

/*
 * Prototype declaration
 */

const struct health_tag_t* health_pedometer_ex950_get_tag(void);

#ifdef __cplusplus
}
#endif

#endif /* !HEALTH_PEDOMETER_EX950_H_ */
