/**
 * \brief    The header file for NFC Healthcare Library. (UW-201NFC)
 * \date     2012/05/11
 * \author   Copyright 2011,2012 Sony Corporation
 */

#include "ics_types.h"
#include "health.h"
#include "health_pedometer.h"

#ifndef HEALTH_PEDOMETER_UW201NFC_H_
#define HEALTH_PEDOMETER_UW201NFC_H_

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Constant
 */

#define HEALTH_NAME_PEDOMETER_UW201NFC                  "UW-201NFC"

#define HEALTH_PEDOMETER_UW201NFC_NORMAL_LOG_DATA_LEN   15
#define HEALTH_PEDOMETER_UW201NFC_DAILY_LOG_DATA_LEN    15
#define HEALTH_PEDOMETER_UW201NFC_HOURLY_LOG_DATA_LEN   24
#define HEALTH_PEDOMETER_UW201NFC_LOG_DATA_TYPE \
    health_pedometer_uw201nfc_data_t

#define HEALTH_PEDOMETER_UW201NFC_PEDOMETER_ID_MAX_LEN  10
#define HEALTH_PEDOMETER_UW201NFC_PRODUCT_CODE_MAX_LEN  16

#define HEALTH_READ_TYPE_UW201NFC_DAILY                 2
#define HEALTH_READ_TYPE_UW201NFC_HOURLY                3
#define HEALTH_READ_TYPE_UW201NFC_LONG                  4
#define HEALTH_READ_TYPE_UW201NFC_LONG_JOG              5

/*
 * Type and structure
 */

typedef struct health_pedometer_uw201nfc_aux_data_t {
    char pedometer_id[HEALTH_PEDOMETER_UW201NFC_PEDOMETER_ID_MAX_LEN + 1];
    char product_code[HEALTH_PEDOMETER_UW201NFC_PRODUCT_CODE_MAX_LEN + 1];
    INT32 weight;       /* Registered weight of the user. */
    INT32 height;       /* Registered height of the user. */
    INT32 age;          /* Registered age of the user. */
    UINT8 sex;          /* Registered sex of the user. (0:female, 1:male) */
    UINT8 low_battery_flag;     /* Battery status. (0:fine, 1:low) */
    UINT16 software_version;    /* Software version of the device. */
} health_pedometer_uw201nfc_aux_data_t;

typedef struct health_pedometer_uw201nfc_data_t {
    health_pedometer_data_t common;
    INT32 jogging_step;         /* The number of jogging steps. */
    INT32 total_calorie;        /* Total calorie. (in unit of a calorie) */
    INT32 burning;              /* The amount of body fat burning.
                                 * (in unit of a gram) */
} health_pedometer_uw201nfc_data_t;

/*
 * Prototype declaration
 */

const struct health_tag_t* health_pedometer_uw201nfc_get_tag(void);

#ifdef __cplusplus
}
#endif

#endif /* !HEALTH_PEDOMETER_UW201NFC_H_ */
