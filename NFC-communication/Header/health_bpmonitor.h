/**
 * \brief    The header file for NFC Healthcare Library.
 *           (Blood Pressure Monitor)
 * \author   Copyright 2012,2013 Sony Corporation
 */

#include "ics_types.h"
#include "utl_calendar.h"
#include "health.h"

#ifndef HEALTH_BPMONITOR_H_
#define HEALTH_BPMONITOR_H_

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Constant
 */

#define HEALTH_CATEGORY_BPMONITOR                       "BloodPressureMonitor"

#define HEALTH_BPMONITOR_LOG_DATA_LEN                   100

#define HEALTH_BPMONITOR_FLAG_PRODUCT                   (1UL << 16)

/*
 * Type and structure
 */

typedef struct health_bpmonitor_data_t {
    ics_calendar_t tm;  /* The measured time. */
    INT32 systolic;     /* Systolic pressure. (in mmHg) */
    INT32 diastolic;    /* Diastolic pressure. (in mmHg) */
    INT32 mean;         /* Mean arterial pressure. (in mmHg) */
    INT32 pulse;        /* Pulse rate. (in beats per minute) */
    UINT32 flags;       /* Information when the data was measured. */
} health_bpmonitor_data_t;

#ifdef __cplusplus
}
#endif

#endif /* !HEALTH_BPMONITOR_H_ */
