/**
 * \brief    The header file for calendar functions.
 * \date     2011/12/25
 * \author   Copyright 2011 Sony Corporation
 */

#include "ics_types.h"

#ifndef UTL_CALENDAR_H_
#define UTL_CALENDAR_H_

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Constant
 */

#define UTL_CALENDAR_EPOCH_YEAR                 1970
/*
 * Type and structure
 */

/**
 * This type represents the time in seconds since the Epoch
 * (00:00:00 UTC, January 1, 1970).
 */
typedef UINT32 ics_calendar_t;

typedef struct ics_date_t {
    INT year;   /* year */
    INT mon;    /* month of year (0 - 11) */
    INT mday;   /* day of month (1 - 31) */
    INT hour;   /* hours (0 - 23) */
    INT min;    /* minutes (0 - 59) */
    INT sec;    /* seconds (0 - 60) */
} ics_date_t;

/*
 * Prototype declaration
 */

UINT32 utl_calendar_get_date(
    ics_calendar_t calendar,
    ics_date_t* date);
UINT32 utl_calendar_set_date(
    ics_calendar_t* calendar,
    const ics_date_t* date);

UINT32 utl_calendar_set_current_local_time(
    ics_calendar_t* calendar);

#ifdef __cplusplus
}
#endif

#endif /* !UTL_CALENDAR_H_ */
