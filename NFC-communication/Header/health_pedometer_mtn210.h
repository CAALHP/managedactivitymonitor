/**
 * \brief    The header file for NFC Healthcare Library. (MTN-210)
 * \author   Copyright 2011,2012,2013 Sony Corporation
 */

#include "ics_types.h"
#include "health.h"
#include "health_pedometer.h"

#ifndef HEALTH_PEDOMETER_MTN210_H_
#define HEALTH_PEDOMETER_MTN210_H_

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Constant
 */

#define HEALTH_NAME_PEDOMETER_MTN210                    "MTN-210"

#define HEALTH_PEDOMETER_MTN210_NORMAL_LOG_DATA_LEN     15
#define HEALTH_PEDOMETER_MTN210_LOG_DATA_TYPE \
    health_pedometer_mtn210_data_t

#define HEALTH_PEDOMETER_MTN210_PEDOMETER_ID_MAX_LEN    16

#define HEALTH_READ_TYPE_MTN210                         2

#define HEALTH_PEDOMETER_MTN210_F_READ_NORMAL           (1 << 0)
#define HEALTH_PEDOMETER_MTN210_F_SET_PERSONAL_DATA     (1 << 8)
#define HEALTH_PEDOMETER_MTN210_F_SET_CLOCK             (1 << 9)
#define HEALTH_PEDOMETER_MTN210_F_READ_SPECIFIC         (1 << 10)

/*
 * Type and structure
 */

typedef struct health_pedometer_mtn210_aux_data_t {
    char pedometer_id[HEALTH_PEDOMETER_MTN210_PEDOMETER_ID_MAX_LEN + 1];
    ics_calendar_t clock;       /* Clock of the pedometer. */
    INT32 weight;       /* Registered weight of the user. */
    INT32 height;       /* Registered height of the user. */
    INT32 age;          /* Registered age of the user. */
    UINT8 sex;          /* Registered sex of the user. (0:female, 1:male) */
    UINT8 err;          /* Error flags. */
    UINT8 low_battery_flag;     /* Battery status. (0:fine to 3:low) */
    INT32 uptime;       /* How long the device has been running in an hour. */
    UINT16 software_version;    /* Software version of the device. */
    UINT16 algorithm_version;   /* Algorithm version of the device. */
} health_pedometer_mtn210_aux_data_t;

typedef struct health_pedometer_mtn210_data_t {
    health_pedometer_data_t common;
    INT32 total_calorie;        /* Total calorie. (in unit of a calorie) */
    INT32 modint_min;           /* Moderate-intensity activity time.
                                 * (in unit of a minute) */
} health_pedometer_mtn210_data_t;

typedef struct health_pedometer_mtn210_read_param_t {
    UINT32 flags;
    ics_calendar_t start;
    UINT32 ndays;
    ics_calendar_t clock;
    INT32 weight;
    INT32 height;
    INT32 age;
    UINT8 sex;
} health_pedometer_mtn210_read_param_t;

/*
 * Prototype declaration
 */

const struct health_tag_t* health_pedometer_mtn210_get_tag(void);

#ifdef __cplusplus
}
#endif

#endif /* !HEALTH_PEDOMETER_MTN210_H_ */
