using namespace std;

#include <stdio.h>  /* printf, fprintf */
#include <stdlib.h> /* strtoul */
#include <string.h> /* strcmp, memset, memcpy */
#include <iostream>
#include <fstream>

#include "Header/felica.h"
#include "Header/felica_basic_lite_s.h"
#include "Header/felica_cc_stub_sdk20.h"
#include "Header/health.h"
#include "Header/health_pedometer.h"
#include "Header/health_bpmonitor.h"
#include "Header/health_glucosemeter.h"
#include "Header/ics_error.h"

#include "UW101NFC.h"

#ifndef DEFAULT_TIMEOUT
#define DEFAULT_TIMEOUT 400 /* ms */
#endif
#ifndef DEFAULT_POLLING_INTERVAL
#define DEFAULT_POLLING_INTERVAL 500 /* ms */
#endif
#ifndef DEFAULT_NUM_OF_READ
#define DEFAULT_NUM_OF_READ 0 /* infinite */
#endif

UW101NFC::UW101NFC(void)
{
	s_timeout = DEFAULT_TIMEOUT;
	s_polling_interval = DEFAULT_POLLING_INTERVAL;
	s_num_of_read = DEFAULT_NUM_OF_READ;
}

UW101NFC::~UW101NFC(void)
{
}

int UW101NFC::Run(void)
{
	bool b;
    UINT32 rc;
    felica_cc_devf_t felica_devf;
    felica_card_t card;
    UINT8 prev_idm[8];
    health_devf_t devf;
    UINT16 system_code;
    UINT32 ii;
    health_tag_data_t* tag_data;
    UINT32 tag_data_len;
    health_tag_data_t* read_data;
    UINT32 n;
    UINT32 buf_size;

    // Print version of the NFC library
    const char* version;
    version = health_get_version();
    printf("Library version: %s\n", version);

    // Allocate buffer
    rc = health_utl_get_tag_data_size(&buf_size);
    if (rc != ICS_ERROR_SUCCESS) {
        fprintf(stderr,"    failure in health_utl_get_tag_data_size():%u\n", rc);
        return 0;
    }

    tag_data = (health_tag_data_t*)malloc(buf_size);
    if (tag_data == NULL) {
        fprintf(stderr, "    failure in malloc()\n");
        return 0;
    }

	// Initialize Felica Library
    rc = felica_cc_stub_sdk20_initialize(&felica_devf, NULL);
    if (rc != ICS_ERROR_SUCCESS) {
        fprintf(stderr, "    failure in felica_cc_stub_initialize():%u\n", rc);
        return 0;
    }

    // Initialize NFC library
    b = initialize_library();
    if (!b) {
        fprintf(stderr, "    failure in initialize_library()\n");
        goto err_free;
    }

	// Enable read/write options
    b = open_reader_writer_auto();
    if (!b) {
        fprintf(stderr, "    failure in open_reader_writer_auto()\n");
        goto err_dispose;
    }

    devf.felica_devf = &felica_devf;
    memset(prev_idm, 0, sizeof(prev_idm));
	
	// Wait for NFC input
    n = 0;
    while ((s_num_of_read == 0) || (n < s_num_of_read)) {
        while (1) {
            const char* category;
            const char* name;
            char* id;
            char* model_name;
            unsigned char polling_system_code[2];
            unsigned char number_of_cards;
            structure_polling polling_param;
            structure_card_information card_info;

            // Start polling data from current device
            system_code = 0xfee1;
            polling_system_code[0] = (unsigned char)(system_code >> 8);
            polling_system_code[1] = (unsigned char)(system_code >> 0);
            polling_param.system_code = polling_system_code;
            polling_param.time_slot = 0x00;
            number_of_cards = 1;
            card_info.card_idm = card.idm;
            card_info.card_pmm = card.pmm;
            b = polling_and_get_card_information(&polling_param, &number_of_cards, &card_info);
            
			if (!b || (number_of_cards == 0)) {
                memset(prev_idm, 0, sizeof(prev_idm));
                break;
            }
            if (memcmp(card.idm, prev_idm, sizeof(prev_idm)) == 0) {
                break;
            }

			// Write IDm to console
            printf("IDm: %02x%02x%02x%02x%02x%02x%02x%02x\n",
                   card.idm[0], card.idm[1], card.idm[2], card.idm[3],
                   card.idm[4], card.idm[5], card.idm[6], card.idm[7]);

			// Ensure that data only is read once
            memcpy(prev_idm, card.idm, sizeof(prev_idm));

            rc = health_utl_set_tag_data(tag_data, &tag_data_len);
            if (rc != ICS_ERROR_SUCCESS) {
                fprintf(stderr,"    failure in health_utl_set_tag_data():%u\n", rc);
                return 0;
            }

			// Lock current transaction
            b = transaction_lock();
            if (!b) {
                fprintf(stderr, "    failure in transaction_lock()\n");
                break;
            }

            // Read health data from current device
            rc = health_tag_read(&devf, system_code, &card, tag_data_len, tag_data, &read_data);

            // Unlock current transaction
            b = transaction_unlock();
            if (!b) {
                fprintf(stderr, "    failure in transaction_unlock()\n");
                goto err_close;
            }

			// Check for support of current device
            if (rc == ICS_ERROR_NOT_SUPPORTED) {
                printf("    Unknown tag.\n");
                break;

			// Check if information reading of the device succeded
            } else if (rc != ICS_ERROR_SUCCESS) {
                fprintf(stderr, "    failure in health_tag_read():%u\n", rc);
                break;
            }

			// Write device category and name of current device to console
            category = health_tag_get_category(read_data);
            name = health_tag_get_name(read_data);
            printf("  Category: %s\n", category);
            printf("  Name: %s\n", name);

			// Write device ID to console
            rc = health_tag_ioctl(read_data, HEALTH_IOCTL_GET_ID, &id);
            if (rc == ICS_ERROR_SUCCESS) {
                printf("  ID: %s\n", id);
            }

			// Write model name to console
            rc = health_tag_ioctl(read_data, HEALTH_IOCTL_GET_MODEL_NAME, &model_name);
            if (rc == ICS_ERROR_SUCCESS) {
                printf("  Model Name: %s\n", model_name);
            }

			// Check if the current device is blood pressure monitor
            if (strcmp(category, HEALTH_CATEGORY_BPMONITOR) == 0) {
                
				// Check whether the tag is capable of getting log data
				rc = health_tag_ioctl(read_data, HEALTH_IOCTL_HAS_CAPABILITY,(void*)HEALTH_CAPABILITY_GET_LOG_DATA_AT);
                if (rc == ICS_ERROR_SUCCESS) {
					
					// Delete old measurements
					remove( "Measurements/bpmonitor.csv" );

					// Define new measurement file
					fstream bpmonitor_file;

                    for (ii = 0; ii < read_data->log_len; ii++) {
                        health_bpmonitor_data_t* bp_data;
                        health_ioctl_get_log_data_at_t param;
                        ics_date_t date;

						// Set the index so that the measurements are listed in ascending order (Newest last)
                        param.index = ii;

						// Read data from current device
                        rc = health_tag_ioctl(read_data, HEALTH_IOCTL_GET_LOG_DATA_AT, &param);
                        if (rc != ICS_ERROR_SUCCESS) {
                            fprintf(stderr,"    failure in " "HEALTH_IOCTL_GET_LOG_DATA_AT:%u\n", rc);
                            goto err_close;
                        }

                        bp_data = (health_bpmonitor_data_t*)(param.log_data);

                        utl_calendar_get_date(bp_data->tm, &date);

						// Write data to console
                        printf("    %04d-%02d-%02d  %d, %d, %d, %d, %02x\n",
                               date.year, date.mon + 1, date.mday,
                               bp_data->systolic, bp_data->diastolic,
                               bp_data->mean, bp_data->pulse,
                               bp_data->flags);

						// Write data to file
						bpmonitor_file.open ("Measurements/bpmonitor.csv", ios::app);
						bpmonitor_file	<< date.year << "," 
										<< (date.mon + 1) << "," 
										<< date.mday << ","
										<< date.hour << ","
										<< date.min << ","
										<< date.sec << ","
										<< bp_data->systolic << "," 
										<< bp_data->diastolic << ","
										<< bp_data->mean << ","
										<< bp_data->pulse << ","
										<< bp_data->flags << "\n";
						bpmonitor_file.close();
                    }
                }

			// Check if the current device is glucose meter
            } else if (strcmp(category, HEALTH_CATEGORY_GLUCOSEMETER) == 0) {
                
				rc = health_tag_ioctl(read_data, HEALTH_IOCTL_HAS_CAPABILITY, (void*)HEALTH_CAPABILITY_GET_LOG_DATA_AT);
                if (rc == ICS_ERROR_SUCCESS) {
                    for (ii = 0; ii < read_data->log_len; ii++) {
                        health_glucosemeter_data_t* glu_data;
                        health_ioctl_get_log_data_at_t param;
                        ics_date_t date;

						// Set the index so that the measurements are listed in ascending order (Newest last)
                        param.index = read_data->log_len - ii - 1;

                        rc = health_tag_ioctl(read_data, HEALTH_IOCTL_GET_LOG_DATA_AT, &param);
                        if (rc != ICS_ERROR_SUCCESS) {
                            fprintf(stderr,"    failure in " "HEALTH_IOCTL_GET_LOG_DATA_AT:%u\n", rc);
                            goto err_close;
                        }

                        glu_data = (health_glucosemeter_data_t*)(param.log_data);

						// Write data to console
                        utl_calendar_get_date(glu_data->tm, &date);
                        printf("    %04d-%02d-%02d  %d, %02x, %02x\n",
                               date.year, date.mon + 1, date.mday,
                               glu_data->blood_glucose, glu_data->unit,
                               glu_data->flags);
                    }
                }
			// Print and log data if the device is a Pedometer
            } else if (strcmp(category, HEALTH_CATEGORY_PEDOMETER) == 0) {
                rc = health_tag_ioctl(
                    read_data, HEALTH_IOCTL_HAS_CAPABILITY,
                    (void*)HEALTH_CAPABILITY_GET_LOG_DATA_AT);
                if (rc == ICS_ERROR_SUCCESS) {

					// Delete old measurements
					remove( "Measurements/pedometer.csv" );

					// Define new measurement file
					fstream pedometer_file;

                    for (ii = 0; ii < read_data->log_len; ii++) {
                        health_pedometer_data_t* pedo_data;
                        health_ioctl_get_log_data_at_t param;
                        ics_date_t date;

						// Set the index so that the measurements are listed in ascending order (Newest last)
						param.index = ii;

                        rc = health_tag_ioctl(read_data, HEALTH_IOCTL_GET_LOG_DATA_AT, &param);
                        if (rc != ICS_ERROR_SUCCESS) {
                            fprintf(stderr,"    failure in " "HEALTH_IOCTL_GET_LOG_DATA_AT:%u\n", rc);
                            goto err_close;
                        }

                        pedo_data = (health_pedometer_data_t*)(param.log_data);

						// Write data to console
                        utl_calendar_get_date(pedo_data->tm, &date);
                        printf("    %04d-%02d-%02d  %d, %d, %d, %d\n",
                               date.year, date.mon + 1, date.mday,
                               pedo_data->step, pedo_data->active_step,
                               pedo_data->calorie, pedo_data->ex);

						// Write data to file
						pedometer_file.open ("Measurements/pedometer.csv", ios::app);
						pedometer_file	<< date.year << "," 
										<< (date.mon + 1) << "," 
										<< date.mday << ","
										<< pedo_data->step << "," 
										<< pedo_data->active_step << ","
										<< pedo_data->calorie << ","
										<< pedo_data->ex << "\n";
						pedometer_file.close();
                    }
                } 
            }

            n++;
            break;
        }

        // Suppress RF output until the next polling
        if (s_polling_interval > 0) {
            b = close_reader_writer();
            if (!b) {
                fprintf(stderr, "    failure in close_reader_writer()\n");
                goto err_dispose;
            }

            Sleep(s_polling_interval);

            b = open_reader_writer_auto();
            if (!b) {
                fprintf(stderr, "    failure in open_reader_writer_auto()\n");
                goto err_dispose;
            }
        }
    }

// Finalize
err_close:
    b = close_reader_writer();
    if (!b) {
        fprintf(stderr, "    failure in close_reader_writer()\n");
    }
err_dispose:
    b = dispose_library();
    if (!b) {
        fprintf(stderr, "    failure in dispose_library()\n");
    }
err_free:
    free(tag_data);
}

void UW101NFC::Setup(void)
{
}