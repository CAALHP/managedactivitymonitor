using namespace std;

#include "UW101NFC.h"

int main(int argc, char* argv[])
{
	UW101NFC pedometer;
 
	int result = pedometer.Run();

	return 0;
}
