#pragma once
#include <stdio.h>  /* printf, fprintf */
#include <stdlib.h> /* strtoul */
#include <string.h> /* strcmp, memset, memcpy */
#include <iostream>
#include <fstream>

#include "Header/felica.h"
#include "Header/felica_basic_lite_s.h"
#include "Header/felica_cc_stub_sdk20.h"
#include "Header/health.h"
#include "Header/health_pedometer.h"
#include "Header/health_bpmonitor.h"
#include "Header/health_glucosemeter.h"
#include "Header/ics_error.h"

class __declspec(dllimport) UW101NFC
{
private:
	UINT32 s_timeout;
	UINT32 s_polling_interval;
	UINT32 s_num_of_read;
public:
	UW101NFC(void);
	~UW101NFC(void);
	int Run();
	void Setup();
};



