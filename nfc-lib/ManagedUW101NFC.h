// ManagedUW101NFC.h

#pragma once
#include "..\NFC-communication\UW101NFC.h"

using namespace System;

namespace nfclib {

	public ref class ManagedUW101NFC
	{
	private: UW101NFC *activityMonitor_;
	public: ManagedUW101NFC();
	public: ~ManagedUW101NFC();
	public: void Run();
	};
}
