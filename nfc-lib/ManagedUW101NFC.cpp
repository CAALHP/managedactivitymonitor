// This is the main DLL file.

#include "stdafx.h"

#include "ManagedUW101NFC.h"

using namespace nfclib;

ManagedUW101NFC::ManagedUW101NFC() : activityMonitor_(new UW101NFC())
{
}

ManagedUW101NFC::~ManagedUW101NFC()
{
	delete activityMonitor_;
}

void ManagedUW101NFC::Run()
{
	activityMonitor_->Run();
}
